from flask import Flask, request
app = Flask(__name__)

@app.route('/books', methods = ['GET', 'POST'])
def all_books_handler():
	if request.method == 'GET':
		return getListOfBooks()
	elif request.method == 'POST':
		return createNewBook()

@app.route('/books/<int:bookid>', methods = ['GET','PUT','DELETE'])
def book_handler(bookid):
	if request.method == 'GET':
		return getBookDetails(bookid)
	elif request.method == 'PUT':
		book_details = None
		return updateBookDetails(bookid,book_details)

	elif request.method == 'DELETE':
		return removeBook(bookid)

def getListOfBooks():
	return "GET - This method will return a list of books."

def createNewBook():

	return "POST - Created new book."

def getBookDetails(bookid):
	return "GET - This method will return the details of a book with id " % bookid

def updateBookDetails(bookid):
	return "PUT - Study with id %s is updated" % bookid

def removeBook(bookid):
	return "DELETE - Study with id %s is removed" % bookid

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5000)
