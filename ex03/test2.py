from flask import Flask, request
app = Flask(__name__)

@app.route('/books/<int:bookid>', methods = ['GET','PUT','DELETE'])
def book_handler(bookid):
	if request.method == 'GET':
		return 1
	elif request.method == 'PUT':
		book_details = None
		return 2
	elif request.method == 'DELETE':
		return 3


if __name__ == '__main__':
	app.run()