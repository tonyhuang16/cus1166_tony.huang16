from flask import Flask
app = Flask(__name__)

@app.route ('/test')
def test1():
	return "program is working"

@app.route('/test/<int:testid>')
def test2(testid):
    return "This url has this id %s" % testid

if __name__ == '__main__':
	app.run()