import requests
import json
import unittest

class TestAPI(unittest.TestCase):
	
	def test_getrequest(self):
		print()
		print("Executing 'GET request' Test ")
		print("______________________________")
		url = "http://127.0.0.1:5000/readHello"
		r = requests.get(url)
		print("Response: " + r.text)
		self.assertIn("GET", r.text)

	def test_putrequest(self):
		print()
		print("Executing 'PUT request' Test ")
		print("-----------------------------")
		url = "http://127.0.0.1:5000/updateHello"
		r = requests.put(url)
		self.assertIn("PUT",r.text)

	def test_deleterequest(self):
		print()
		print("Executing 'DELETE request' Test ")
		print("-------------------------------")
		url = "http://127.0.0.1:5000/deleteHello"
		r = requests.delete(url)
		print("Response: " + r.text)
		self.assertIn("DELETE",r.text)