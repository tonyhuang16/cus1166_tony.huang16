from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello():
	return "<h1>Hello CUS1166 class!!!!</h1>"

@app.route('/readHello')
def getRequestHello():
	return "Hello, You sent a GET Request!"

@app.route('/createHello', methods = ['POST'])
def postRequestHello():
	return "Hello, you sent a POST message"

@app.route('/updateHello', methods = ['PUT'])
def updateRequestHello():
	return "Hello, you sent a PUT request!"

@app.route('/deleteHello', methods = ['DELETE'])
def deleteRequestHello():
	return "Hello, you sent a received a DELETE request!"
	
if __name__ == '__main__':
	app.run()